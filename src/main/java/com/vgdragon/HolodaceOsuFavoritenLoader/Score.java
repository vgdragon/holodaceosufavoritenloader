package com.vgdragon.HolodaceOsuFavoritenLoader;

import java.util.Date;
import java.util.EnumSet;

/**
 * Created by Dragonking3x on 24.01.2017.
 */
public class Score
{
    public OsuReader.GameplayMode mode;
    public int version; // 20150203
    public String beatmapMd5Hash;
    public String playerName;
    public String replayMd5Hash;
    public short numberOf300s;
    public short numberOf100s;
    public short numberOf50s;
    public short numberOfGekis;
    public short numberOfKatus;
    public short numberOfMisses;
    public int replayScore;
    public short maxCombo;
    public boolean perfectCombo;
    public EnumSet<OsuReader.OsuMod> modsUsed;
    public String unknown1;
    public Date timestamp;
    public int unknown2;
    public int unknown3;
    public int unknown4;
}
