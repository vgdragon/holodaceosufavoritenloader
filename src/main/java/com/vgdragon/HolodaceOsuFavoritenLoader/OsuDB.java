package com.vgdragon.HolodaceOsuFavoritenLoader;

import java.util.Date;
import java.util.List;

/**
 * Created by Dragonking3x on 24.01.2017.
 */
public class OsuDB
{
    public int version; // 20150204
    public int folderCount;
    public boolean accountUlocked;
    public Date unlockDate;
    public String playerName;
    public List<BeatmapOsuDB> beatmapsOsuDB;
}
