package com.vgdragon.HolodaceOsuFavoritenLoader;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dragonking3x on 26.01.2017.
 */
public class UI {
    private JTextField osuFolderTextArea;
    private JButton osuFolderButton;
    private JTextField holodanceFolderTextArea;
    private JButton holodanceFolderButton;
    private JButton creatFavoFilesButton;
    private JList favoList;
    private JButton setFavoButton;
    private JPanel panel;
    private JTextArea infoText;

    private static String osuFolder;
    private static String holodanceFolder;
    private static Map<String, Integer> favoFolderName;

    private static Thread favoSearcher;

    public static void ui(){


        getDataFromTxt();




        /////////////////////////

        JFrame frame = new JFrame("osu! Favo importer");
        frame.setContentPane(new UI().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    public UI() {

        infoText.setText("waiting");
        osuFolderTextArea.setText(String.valueOf(osuFolder));
        holodanceFolderTextArea.setText(String.valueOf(holodanceFolder));


        osuFolderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(osuFolder));
                fileChooser.setDialogTitle("Osu Folder");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);

                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

                    osuFolder = String.valueOf(fileChooser.getSelectedFile());
                    osuFolderTextArea.setText(String.valueOf(osuFolder));

                }

            }
        });


        holodanceFolderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(holodanceFolder));
                fileChooser.setDialogTitle("Holodance Folder");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);

                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

                    holodanceFolder = String.valueOf(fileChooser.getSelectedFile());
                    holodanceFolderTextArea.setText(String.valueOf(holodanceFolder));

                }
            }
        });
        creatFavoFilesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File testFile01 = new File(osuFolder + "\\collection.db");
                File testFile02 = new File(osuFolder + "\\osu!.db");
                File testFile03 = new File(holodanceFolder + "\\GameData");

                if (testFile01.exists() && testFile02.exists() && testFile03.exists()) {


                    if (favoSearcher == null) {




                        infoText.setText("loading favo");

                        favoSearcher = new Thread() {
                            public void run() {
                                creatFavoFilesButton.setEnabled(false);
                                try {
                                    OsuReader.osureader(osuFolder, holodanceFolder);
                                    getDataFromTxt();

                                    osuFolderTextArea.setText(String.valueOf(osuFolder));
                                    holodanceFolderTextArea.setText(String.valueOf(holodanceFolder));
                                    infoText.setText("done");

                                    Collection<String> values = favoFolderName.keySet();
                                    DefaultListModel listModel = new DefaultListModel();
                                    for (String favoName : values) {
                                        listModel.addElement(favoName);
                                    }
                                    favoList.setModel(listModel);
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                creatFavoFilesButton.setEnabled(true);
                            }

                        };
                        favoSearcher.start();


                    } else {
                        if (!favoSearcher.isAlive()) {



                            infoText.setText("loading favo");

                            favoSearcher = new Thread() {
                                public void run() {
                                    creatFavoFilesButton.setEnabled(false);
                                    try {
                                        OsuReader.osureader(osuFolder, holodanceFolder);
                                        getDataFromTxt();

                                        osuFolderTextArea.setText(String.valueOf(osuFolder));
                                        holodanceFolderTextArea.setText(String.valueOf(osuFolder));
                                        infoText.setText("done");

                                        Collection<String> values = favoFolderName.keySet();
                                        DefaultListModel listModel = new DefaultListModel();
                                        for (String favoName : values) {
                                            listModel.addElement(favoName);
                                        }
                                        favoList.setModel(listModel);
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }
                                    creatFavoFilesButton.setEnabled(true);
                                }

                            };
                            favoSearcher.start();


                        }
                    }


                } else {
                    if (!testFile01.exists() || !testFile02.exists()) {
                        infoText.setText("osu! Folder is wrong");
                    } else if (!testFile03.exists()) {
                        infoText.setText("holodance Folder is wrong");
                    }

                }
            }
        });


        setFavoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String selectedFavo = favoList.getSelectedValue().toString();

                int favoInt = favoFolderName.get(selectedFavo);
                String outputFolder = holodanceFolder + "\\GameData";


                File currentFile = new File(outputFolder + "\\FileSystemCache_Osu_Favorites.json");
                File originalFile = new File(outputFolder + "\\FileSystemCache_Osu_Favorites000.json");

                if (!originalFile.exists()) {


                    try {
                        FileUtils.copyFile(currentFile, originalFile);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                }

                if (favoInt < 10) {
                    originalFile = new File(outputFolder + "\\FileSystemCache_Osu_Favorites00" + favoInt + ".json");
                } else if (favoInt < 100) {
                    originalFile = new File(outputFolder + "\\FileSystemCache_Osu_Favorites0" + favoInt + ".json");
                } else if (favoInt < 1000) {
                    originalFile = new File(outputFolder + "\\FileSystemCache_Osu_Favorites" + favoInt + ".json");
                }

                try {
                    FileUtils.copyFile(originalFile, currentFile);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }


            }
        });

        Collection<String> values = favoFolderName.keySet();
        DefaultListModel listModel = new DefaultListModel();
        for (String favoName : values) {
            listModel.addElement(favoName);
        }
        favoList.setModel(listModel);


    }

    public static void getDataFromTxt(){
        Map<String, Integer> folderName = new HashMap<String, Integer>();
        folderName.put("Holodance Original", 0);
        File txtData = new File("Data.txt");

        if(!txtData.exists()){
            String temp = "osuFolder: D:\\osu!" + System.lineSeparator() +
                    "holodanceFolder: D:\\steam\\steamapps\\Holodance";
            osuFolder = "D:\\osu!";
            holodanceFolder = "D:\\steam\\steamapps\\Holodance";
            System.out.println("test");


            try {

                FileUtils.writeStringToFile(txtData, temp,"UTF-8");

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("test02");

        } else {
            String txtDataString = null;
            try {
                txtDataString = FileUtils.readFileToString(txtData, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] splitInhalt = txtDataString.split(System.lineSeparator());


            for (int i = 0; i < splitInhalt.length; i++) {
                String sort = splitInhalt[i];
                sort = sort.trim();
                String sort02[] = sort.split(":");

                if(sort02.length > 2){
                    for (int ii = 1; ii < sort02.length; ii++) {
                        if(ii != 1){
                            sort02[1] = sort02[1] + ":" + sort02[ii];
                        }
                    }
                }


                if (sort02[0].equalsIgnoreCase("osuFolder") && (sort02.length != 1)) {
                    String sort03 = sort02[1];
                    osuFolder = sort03.trim();
                } else if (sort02[0].equalsIgnoreCase("holodanceFolder") && (sort02.length != 1)) {
                    String sort03 = sort02[1];
                    holodanceFolder = sort03.trim();
                } else {
                    splitInhalt[i] = splitInhalt[i].trim();
                    if(!splitInhalt[i].equalsIgnoreCase("")){
                        sort02[0] =  sort02[0].trim();
                        if(StringUtils.isNumeric(sort02[0])) {

                            int putItIn = Integer.parseInt(sort02[0]);


                            folderName.put(sort02[1], putItIn);
                        }
                    }
                }



            }

        }

        favoFolderName = folderName;
    }


}
