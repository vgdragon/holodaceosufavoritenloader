package com.vgdragon.HolodaceOsuFavoritenLoader;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dragonking3x on 24.01.2017.
 */
public class BeatmapOsuDB
{
    public int size;
    public String artistName;
    public String artistNameUnicode;
    public String songtitel;
    public String songtitelUnicode;
    public String creatorName;
    public String difficulty;
    public String audioFileName;
    public String beatmapMd5Hash;
    public String osufileName;
    public OsuReader.RankedStatus rankedStatus;
    public short numberHitcircles;
    public short numbersliders;
    public short numberSpinners;
    public Date lastModificationDate;

    public byte approachRate;
    public float approachRateSingle;
    public byte CircleSize;
    public float CircleSizeSingle;
    public byte hpDrain;
    public float hpDrainSingle;
    public byte overallDifficulty;
    public float overallDifficultySingle;

    public double sliderVelocity;

    public Map<Integer, Double> starRatingInfoForOsu = new HashMap<Integer, Double>();


    public Map<Integer, Double> starRatingInfoForTaiko = new HashMap<Integer, Double>();

    public Map<Integer, Double> starRatingInfoForCTB = new HashMap<Integer, Double>();

    public  Map<Integer, Double> starRatingInfoForMania = new HashMap<Integer, Double>();


    public int drainTime;
    public int totalTime;
    public int timeWhenAudioPreview;
    public int timePoints;
    public List<Double> timePointsList;
    public int beatbapID;
    public int beatmapSetID;
    public int threadID;
    public byte gradeAchievedInOsu;
    public byte gradeAchievedInTaiko;
    public byte gradeAchievedInCTB;
    public byte gradeAchievedInMania;
    public short beatmapOfset;
    public float stackLeniency;
    public OsuReader.GameplayMode osuMod;
    public String songSource;
    public String songTags;
    public short onlineOffset;
    public String fontUsedTitel;
    public boolean unplayed;
    public Date lastTimePlayed;
    public boolean isOsz2;
    public String folderName;
    public Date lastTimeCheckedAgainsRepository;
    public boolean ignorBeatmapSound;
    public boolean ignorBeatmapSkin;
    public boolean disableBeatmapStoryboard;
    public boolean disableBeatmapVideo;
    public boolean visualOverwrite;
    public short unknow;
    public int lastModificationTime;
    public byte maniaScrollSpeed;


}
