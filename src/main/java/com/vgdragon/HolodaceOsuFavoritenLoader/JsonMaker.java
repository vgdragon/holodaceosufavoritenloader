package com.vgdragon.HolodaceOsuFavoritenLoader;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dragonking3x on 25.01.2017.
 */
public class JsonMaker {

    public void JsonMaker(List<FavoBeatmapList> favoBeatmapListList, String songfolder, String outputFolder, String osuFolder, String holodanceFolder){

        List<String> favoJsonStringList = new ArrayList<String>();
        String txtOutput = "";
        List<String> nameList = new ArrayList<String>();

        for (int i = 0;i < favoBeatmapListList.size(); i++){
            FavoBeatmapList favoBeatmapList = favoBeatmapListList.get(i);
            String jsonString = "{\"version\":4,\"favoriteBeatmaps\":[";

            for(int ii = 0; ii < favoBeatmapList.collection.size(); ii++){
                BeatmapOsuDB beatmapOsuDB = favoBeatmapList.collection.get(ii);
                if(ii == favoBeatmapList.collection.size() - 1){
                    String approachRate = "";
                    if(beatmapOsuDB.approachRate == 0){
                        approachRate = Float.toString(beatmapOsuDB.approachRateSingle);
                    }else {
                        approachRate = Byte.toString(beatmapOsuDB.approachRate);
                    }
                    String difficultiy = "";
                    if(beatmapOsuDB.overallDifficulty != 0x00){
                        difficultiy = Byte.toString(beatmapOsuDB.overallDifficulty);
                    }else if (beatmapOsuDB.overallDifficultySingle != 0x00){
                        difficultiy = Float.toString(beatmapOsuDB.overallDifficultySingle);
                    } else {
                        difficultiy = "0.0";
                    }


                    jsonString = jsonString + "{" +

                            "\"fileName\":\""+ songfolder + "\\\\" + beatmapOsuDB.folderName + "\\\\" + beatmapOsuDB.osufileName + "\"," +  //D:\\holodance\\147962 UPLIFT SPICE - Omega Rhythm\\UPLIFT SPICE - Omega Rhythm (Jemmmmy) [Normal].osu\"" +
                            "\"audioFile\":\""+ songfolder + "\\\\" + beatmapOsuDB.folderName + "\\\\" + beatmapOsuDB.audioFileName + "\"," +  //D:\\holodance\\147962 UPLIFT SPICE - Omega Rhythm\\UPLIFT SPICE-01 - .mp3\"" +
                            "\"artist\":\""+ beatmapOsuDB.artistName + "\"," +  //UPLIFT SPICE\"" +
                            "\"durationSeconds\":"+ (float)(beatmapOsuDB.totalTime / 1000) + "," +  //0" +
                            "\"playSessions\":"+ "[]" + "," +  //[]" +
                            "\"title\":\""+ beatmapOsuDB.songtitel + "\"," +  //Omega Rhythm\"" +
                            "\"lastPlayed\":"+ "0" + "," +  //0" +
                            "\"approachRate\":"+ approachRate + "," +  //0" +
                            "\"spinnerCount\":"+ beatmapOsuDB.numberSpinners + "," +  //0" +
                            "\"score\":"+ "0" + "," +  //0,\"bpmMax\":\""+  + "\"," +  //1000" +
                            "\"bpmChanges\":"+ "0" + "," +  //0" +
                            "\"audioPreviewTime\":"+ (float)(beatmapOsuDB.timeWhenAudioPreview / 1000) + "," +  //0" +
                            "\"maxTimeBetweenEvents\":"+ "0.0" + "," +  //0" +
                            "\"bpm\":"+ "0" + "," +  //120" +
                            "\"mapGapsGreater10Seconds\":"+ "0" + "," +  //0" +
                            "\"creator\":\""+ beatmapOsuDB.creatorName + "\"," +  //Jemmmmy\"" +
                            "\"bpmMin\":"+ "1000" + "," +  //1000" +
                            "\"bpmMax\":"+ "1000" + "," +  //1000" +
                            "\"beatmapID\":"+ beatmapOsuDB.beatbapID + "," +  //366143" +
                            "\"mapGapsGreater30Seconds\":"+ "0" + "," +  //0" +
                            "\"startedCount\":"+ "0" + "," +  //0" +
                            "\"maxScore\":"+ "0" + "," +  //0" +
                            "\"version\":\""+ beatmapOsuDB.difficulty + "\"," +  //Normal\"" +
                            "\"sliderCount\":"+ beatmapOsuDB.numbersliders + "," +
                            "\"difficulty\":"+ difficultiy + "," +
                            "\"finishedCount\":"+ "0" + "," +  //0" +
                            "\"orbCount\":"+ beatmapOsuDB.numberHitcircles + "," +  //0" +
                            "\"beatmapSetID\":"+ beatmapOsuDB.beatmapSetID + "," +  //147962" +
                            "\"lastModified\":"+ beatmapOsuDB.lastModificationDate.getTime() + "," +  //635612189240898615" +
                            "\"gameMode\":"+ beatmapOsuDB.osuMod.byteValue  +  //0"

                            "}]}";

                } else {
                    String approachRate = "";
                    if(beatmapOsuDB.approachRate == 0){
                        approachRate = Float.toString(beatmapOsuDB.approachRateSingle);
                    }else {
                        approachRate = Byte.toString(beatmapOsuDB.approachRate);
                    }
                    String difficultiy = "";
                    if(beatmapOsuDB.overallDifficulty != 0x00){
                        difficultiy = Byte.toString(beatmapOsuDB.overallDifficulty);
                    }else if (beatmapOsuDB.overallDifficultySingle != 0x00){
                        difficultiy = Float.toString(beatmapOsuDB.overallDifficultySingle);
                    } else {
                        difficultiy = "0.0";
                    }


                    jsonString = jsonString + "{" +

                            "\"fileName\":\""+ songfolder + "\\\\" + beatmapOsuDB.folderName + "\\\\" + beatmapOsuDB.osufileName + "\"," +  //D:\\holodance\\147962 UPLIFT SPICE - Omega Rhythm\\UPLIFT SPICE - Omega Rhythm (Jemmmmy) [Normal].osu\"" +
                            "\"audioFile\":\""+ songfolder + "\\\\" + beatmapOsuDB.folderName + "\\\\" + beatmapOsuDB.audioFileName + "\"," +  //D:\\holodance\\147962 UPLIFT SPICE - Omega Rhythm\\UPLIFT SPICE-01 - .mp3\"" +
                            "\"artist\":\""+ beatmapOsuDB.artistName + "\"," +  //UPLIFT SPICE\"" +
                            "\"durationSeconds\":"+ (float)(beatmapOsuDB.totalTime / 1000) + "," +  //0" +
                            "\"playSessions\":"+ "[]" + "," +  //[]" +
                            "\"title\":\""+ beatmapOsuDB.songtitel + "\"," +  //Omega Rhythm\"" +
                            "\"lastPlayed\":"+ "0" + "," +  //0" +
                            "\"approachRate\":"+ approachRate + "," +  //0" +
                            "\"spinnerCount\":"+ beatmapOsuDB.numberSpinners + "," +  //0" +
                            "\"score\":"+ "0" + "," +  //0,\"bpmMax\":\""+  + "\"," +  //1000" +
                            "\"bpmChanges\":"+ "0" + "," +  //0" +
                            "\"audioPreviewTime\":"+ (float)(beatmapOsuDB.timeWhenAudioPreview / 1000) + "," +  //0" +
                            "\"maxTimeBetweenEvents\":"+ "0.0" + "," +  //0" +
                            "\"bpm\":"+ "0" + "," +  //120" +
                            "\"mapGapsGreater10Seconds\":"+ "0" + "," +  //0" +
                            "\"creator\":\""+ beatmapOsuDB.creatorName + "\"," +  //Jemmmmy\"" +
                            "\"bpmMin\":"+ "1000" + "," +  //1000" +
                            "\"bpmMax\":"+ "1000" + "," +  //1000" +
                            "\"beatmapID\":"+ beatmapOsuDB.beatbapID + "," +  //366143" +
                            "\"mapGapsGreater30Seconds\":"+ "0" + "," +  //0" +
                            "\"startedCount\":"+ "0" + "," +  //0" +
                            "\"maxScore\":"+ "0" + "," +  //0" +
                            "\"version\":\""+ beatmapOsuDB.difficulty + "\"," +  //Normal\"" +
                            "\"sliderCount\":"+ beatmapOsuDB.numbersliders + "," +
                            "\"difficulty\":"+ difficultiy + "," +
                            "\"finishedCount\":"+ "0" + "," +  //0" +
                            "\"orbCount\":"+ beatmapOsuDB.numberHitcircles + "," +  //0" +
                            "\"beatmapSetID\":"+ beatmapOsuDB.beatmapSetID + "," +  //147962" +
                            "\"lastModified\":"+ beatmapOsuDB.lastModificationDate.getTime() + "," +  //635612189240898615" +
                            "\"gameMode\":"+ beatmapOsuDB.osuMod.byteValue +  //0"

                            "},";
                }
            }
            favoJsonStringList.add(jsonString);
            nameList.add(favoBeatmapListList.get(i).name);
            if(i + 1 < 10){
                txtOutput = txtOutput +"FileSystemCache_Osu_Favorites00" + (i + 1) + ".json = " + favoBeatmapListList.get(i).name + System.lineSeparator();
            } else if(i + 1 < 100){
                txtOutput = txtOutput +"FileSystemCache_Osu_Favorites0" + (i + 1) + ".json = " + favoBeatmapListList.get(i).name + System.lineSeparator();
            } else if(i + 1 < 1000){
                txtOutput = txtOutput +"FileSystemCache_Osu_Favorites" + (i + 1) + ".json = " + favoBeatmapListList.get(i).name + System.lineSeparator();
            }


        }
        File filetxt = new File(outputFolder + "\\" + "OsuFavoritesList.txt");

        try {
            FileUtils.writeStringToFile(filetxt, txtOutput,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String temp = "osuFolder: " + osuFolder + System.lineSeparator() +
                "holodanceFolder: " + holodanceFolder;

        for(int i = 0; i < favoJsonStringList.size(); i++){

            if(i + 1 < 10){
                File file = new File(outputFolder + "\\" + "FileSystemCache_Osu_Favorites00" + (i + 1) + ".json");

                try {
                    FileUtils.writeStringToFile(file, favoJsonStringList.get(i),"UTF-8");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(i + 1 < 100){
                File file = new File(outputFolder + "\\" + "FileSystemCache_Osu_Favorites0" + (i + 1) + ".json");

                try {
                    FileUtils.writeStringToFile(file, favoJsonStringList.get(i),"UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(i + 1 < 1000){
                File file = new File(outputFolder + "\\" + "FileSystemCache_Osu_Favorites" + (i + 1) + ".json");

                try {
                    FileUtils.writeStringToFile(file, favoJsonStringList.get(i),"UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            temp = temp + System.lineSeparator() + (i + 1) + ":" + nameList.get(i);
        }



        File txtData = new File("Data.txt");

        try {
            FileUtils.writeStringToFile(txtData, temp,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}


