package com.vgdragon.HolodaceOsuFavoritenLoader;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

public class OsuReader
{
    public static void osureader(String osuFolder, String holodanceFolder) throws IOException {

        String[] arg = new String[3];
        //arg[0] = "collection";
        //arg[1] = "D:\\osu!\\collection.db";
        //arg[0] = "osu!";
        //arg[1] = "D:\\osu!\\osu!.db";
        //arg[0] = "scores";
        //arg[1] = "D:\\osu!\\scores.db";


        //arg[0] = "D:\\osu!\\collection.db";
        //arg[1] = "D:\\osu!\\osu!.db";
        //arg[2] = "C:\\steam\\steamapps\\Holodance\\GameData\\FileSystemCache_Osu_Favorites.json";
        //String songFolder = "D:\\\\osu!\\\\Songs";
        //String favoName = "";
        //String outputFolder = "C:\\steam\\steamapps\\Holodance\\GameData";

        String osuCollectionDBFile = osuFolder + "\\collection.db";
        String osuDBFile = osuFolder + "\\osu!.db";
        //arg[2] = args[1] + "\\GameData\\FileSystemCache_Osu_Favorites.json";
        String songFolder =  osuFolder.replaceAll("\\\\","\\\\\\\\") + "\\\\Songs";
        String favoName = "";
        String outputFolder = holodanceFolder + "\\GameData";


        //File file = new File(arg[2]);
        //String holodanceFavoFile = FileUtils.readFileToString(file, "UTF-8");

        //JSONObject jsonObject = new JSONObject(holodanceFavoFile);
        //JSONArray jsonArgs = jsonObject.getJSONArray("favoriteBeatmaps");

        OsuReader reader = new OsuReader(osuCollectionDBFile);

        CollectionDB favo = reader.readCollectionDB();
        //for (CollectionItem item : favo.collections)
        //{
        //    System.out.println();
        //    System.out.printf("Name: %s\n", item.name);
        //    for (String hash : item.md5Hashes)
        //    {
        //        System.out.printf("  Hash: %s\n", hash);
//
//
//
//
        //    }
        //}

        reader = new OsuReader(osuDBFile);

        OsuDB osuDB = reader.readOsuDB();
        //List<BeatmapOsuDB> favoList = new ArrayList<BeatmapOsuDB>();
        //List<String> workhHash = new ArrayList<String>();
        List<CollectionItem> collectionItemList = new ArrayList<CollectionItem>();
        for(int i = 0; i < favo.collections.size(); i++){
            //if(favoName.equalsIgnoreCase("")) {
                collectionItemList.add(favo.collections.get(i));
            //} else {
            //    if (favo.collections.get(i).name.equalsIgnoreCase(favoName)) {
            //        workhHash = favo.collections.get(i).md5Hashes;
//
//
            //        i = favo.collections.size();
//
            //    }
            //}

        }

        List<FavoBeatmapList> favoBeatmapListList = new ArrayList<FavoBeatmapList>();

        //if(favoName.equalsIgnoreCase("")) {
            for (int i = 0; i < collectionItemList.size(); i++) {
                FavoBeatmapList favoBeatmapList = new FavoBeatmapList();
                favoBeatmapList.name = collectionItemList.get(i).name;
                List<BeatmapOsuDB> beatmapOsuDBList = new ArrayList<BeatmapOsuDB>();
                for(int ii = 0; ii < collectionItemList.get(i).md5Hashes.size(); ii++) {

                    for (int iii = 0; iii < osuDB.beatmapsOsuDB.size(); iii++) {
                        if (osuDB.beatmapsOsuDB.get(iii).beatmapMd5Hash.equalsIgnoreCase(collectionItemList.get(i).md5Hashes.get(ii))) {

                            beatmapOsuDBList.add(osuDB.beatmapsOsuDB.get(iii));
                            iii = osuDB.beatmapsOsuDB.size();
                        }
                    }
                }
                if(beatmapOsuDBList.size() > 0){
                    favoBeatmapList.collection = beatmapOsuDBList;
                    favoBeatmapListList.add(favoBeatmapList);
                }
            }

            new JsonMaker().JsonMaker(favoBeatmapListList, songFolder, outputFolder, osuFolder, holodanceFolder);


        //} else {
        //    for (int i = 0; i < workhHash.size(); i++) {
        //        for (int ii = 0; ii < osuDB.beatmapsOsuDB.size(); ii++) {
        //            if (osuDB.beatmapsOsuDB.get(ii).beatmapMd5Hash.equalsIgnoreCase(workhHash.get(i))) {
        //                favoList.add(osuDB.beatmapsOsuDB.get(ii));
        //                ii = osuDB.beatmapsOsuDB.size();
        //            }
        //        }
        //    }
        //}




    }

    private DataInputStream reader;

    private int id;

    public OsuReader(String filename) throws IOException
    {
        this(new FileInputStream(filename));
    }

    public OsuReader(InputStream source)
    {
        this.reader = new DataInputStream(source);
    }

    // --- Primitive values ---

    public byte readByte() throws IOException
    {
        // 1 byte
        return this.reader.readByte();
    }

    public short readShort() throws IOException
    {
        // 2 bytes, little endian
        byte[] bytes = new byte[2];
        this.reader.readFully(bytes);
        ByteBuffer bb = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return bb.getShort();
    }

    public int readInt() throws IOException
    {
        // 4 bytes, little endian
        byte[] bytes = new byte[4];
        this.reader.readFully(bytes);
        ByteBuffer bb = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

    public long readLong() throws IOException
    {
        // 8 bytes, little endian
        byte[] bytes = new byte[8];
        this.reader.readFully(bytes);
        ByteBuffer bb = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return bb.getLong();
    }

    public int readULEB128() throws IOException
    {
        // variable bytes, little endian
        // MSB says if there will be more bytes. If cleared,
        // that byte is the last.
        int value = 0;
        for (int shift = 0; shift < 32; shift += 7)
        {
            byte b = this.reader.readByte();
            value |= ((int) b & 0x7F) << shift;

            if (b >= 0) return value; // MSB is zero. End of value.
        }
        throw new IOException("ULEB128 too large");
    }

    public float readSingle() throws IOException
    {
        // 4 bytes, little endian
        byte[] bytes = new byte[4];
        this.reader.readFully(bytes);
        ByteBuffer bb = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return bb.getFloat();
    }

    public double readDouble() throws IOException
    {
        // 8 bytes little endian
        byte[] bytes = new byte[8];
        this.reader.readFully(bytes);
        ByteBuffer bb = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        return bb.getDouble();
    }

    public boolean readBoolean() throws IOException
    {
        // 1 byte, zero = false, non-zero = true
        return this.reader.readBoolean();
    }

    public String readString() throws IOException
    {
        // variable length
        // 00 = empty string
        // 0B <length> <char>* = normal string
        // <length> is encoded as an LEB, and is the byte length of the rest.
        // <char>* is encoded as UTF8, and is the string content.
        byte kind = this.reader.readByte();
        if (kind == 0) return "";
        if (kind != 11)
        {
            throw new IOException(String.format("String format error: Expected 0x0B or 0x00, found 0x%02X", (int) kind & 0xFF));
        }
        int length = readULEB128();
        if (length == 0) return "";
        byte[] utf8bytes = new byte[length];
        this.reader.readFully(utf8bytes);
        return new String(utf8bytes, "UTF-8");
    }

    public Date readDate() throws IOException
    {
        long ticks = readLong();
        long TICKS_AT_EPOCH = 621355968000000000L;
        long TICKS_PER_MILLISECOND = 10000;

        return new Date((ticks - TICKS_AT_EPOCH)/TICKS_PER_MILLISECOND);
    }

    // --- Composite structures ---

    public CollectionDB readCollectionDB() throws IOException
    {
        CollectionDB result = new CollectionDB();
        result.version = readInt();
        int count = readInt();
        result.collections = new ArrayList<CollectionItem>(count);
        for (int i = 0; i < count; i++)
        {
            CollectionItem item = readCollectionItem();
            result.collections.add(item);
        }
        return result;
    }

    public CollectionItem readCollectionItem() throws IOException
    {
        CollectionItem item = new CollectionItem();
        item.name = readString();
        int count = readInt();
        item.md5Hashes = new ArrayList<String>(count);
        for (int i = 0; i < count; i++)
        {
            String md5Hash = readString();
            item.md5Hashes.add(md5Hash);
        }
        return item;
    }

    public ScoresDB readScoresDB() throws IOException
    {
        ScoresDB result = new ScoresDB();
        result.version = readInt();
        int count = readInt();
        result.beatmaps = new ArrayList<Beatmap>(count);
        for (int i = 0; i < count; i++)
        {
            Beatmap beatmap = readBeatmap();
            result.beatmaps.add(beatmap);
        }
        return result;
    }
    public OsuDB readOsuDB() throws IOException
    {
        OsuDB result = new OsuDB();
        result.version = readInt();
        result.folderCount = readInt();
        result.accountUlocked = readBoolean();
        result.unlockDate = readDate();

        result.playerName = readString();

        int count = readInt();

        result.beatmapsOsuDB = new ArrayList<BeatmapOsuDB>(count);
        for (int i = 0; i < count; i++)
        {
            BeatmapOsuDB beatmapOsuDB = readBeatmapOsuDB(result.version);
            result.beatmapsOsuDB.add(beatmapOsuDB);
        }
        return result;
    }

    public Beatmap readBeatmap() throws IOException
    {
        Beatmap result = new Beatmap();
        result.md5Hash = readString();
        int count = readInt();
        result.scores = new ArrayList<Score>();
        for (int i = 0; i < count; i++)
        {
            Score score = readScore();
            result.scores.add(score);
        }
        return result;
    }


    public Score readScore() throws IOException
    {
        Score result = new Score();
        result.mode = GameplayMode.valueOf(readByte(), id);
        result.version = readInt();
        result.beatmapMd5Hash = readString();
        result.playerName = readString();
        result.replayMd5Hash = readString();
        result.numberOf300s = readShort();
        result.numberOf100s = readShort();
        result.numberOf50s = readShort();
        result.numberOfGekis = readShort();
        result.numberOfKatus = readShort();
        result.numberOfMisses = readShort();
        result.replayScore = readInt();
        result.maxCombo = readShort();
        result.perfectCombo = readBoolean();
        result.modsUsed = OsuMod.valueOf(readInt());
        result.unknown1 = readString();
        result.timestamp = readDate();
        result.unknown2 = readInt();
        result.unknown3 = readInt();
        result.unknown4 = readInt();
        return result;
    }

    public BeatmapOsuDB readBeatmapOsuDB(int version) throws IOException{
        BeatmapOsuDB beatmapOsuDB = new BeatmapOsuDB();

        beatmapOsuDB.size = readInt();
        beatmapOsuDB.artistName = readString();
        beatmapOsuDB.artistNameUnicode = readString();
        beatmapOsuDB.songtitel = readString();
        beatmapOsuDB.songtitelUnicode = readString();
        beatmapOsuDB.creatorName = readString();
        beatmapOsuDB.difficulty = readString();
        beatmapOsuDB.audioFileName = readString();
        beatmapOsuDB.beatmapMd5Hash = readString();
        beatmapOsuDB.osufileName = readString();
        beatmapOsuDB.rankedStatus = RankedStatus.valueOf(readByte());
        beatmapOsuDB.numberHitcircles = readShort();
        beatmapOsuDB.numbersliders = readShort();
        beatmapOsuDB.numberSpinners = readShort();
        beatmapOsuDB.lastModificationDate = readDate();

        if(version < 20140609) {
            beatmapOsuDB.approachRate = readByte();
            beatmapOsuDB.CircleSize = readByte();
            beatmapOsuDB.hpDrain = readByte();
            beatmapOsuDB.overallDifficulty = readByte();
        } else {
            beatmapOsuDB.approachRateSingle = readSingle();
            beatmapOsuDB.CircleSizeSingle = readSingle();
            beatmapOsuDB.hpDrainSingle = readSingle();
            beatmapOsuDB.overallDifficultySingle = readSingle();
        }
        beatmapOsuDB.sliderVelocity = readDouble();

        if(version >= 20140609) {
            int counter = readInt();
            for(int i = 0; i < counter; i++) {
                byte f = readByte();
                int difficoult = readInt();
                byte g = readByte();
                double difficoultStar = readDouble();
                beatmapOsuDB.starRatingInfoForOsu.put(difficoult, difficoultStar);
            }

            counter = readInt();
            for(int i = 0; i < counter; i++) {
                readByte();
                int difficoult = readInt();
                readByte();
                double difficoultStar = readDouble();
                beatmapOsuDB.starRatingInfoForTaiko.put(difficoult, difficoultStar);
            }
            counter = readInt();
            for(int i = 0; i < counter; i++) {
                readByte();
                int difficoult = readInt();
                readByte();
                double difficoultStar = readDouble();
                beatmapOsuDB.starRatingInfoForCTB.put(difficoult, difficoultStar);
            }
            counter = readInt();
            for(int i = 0; i < counter; i++) {
                readByte();
                int difficoult = readInt();
                readByte();
                double difficoultStar = readDouble();
                beatmapOsuDB.starRatingInfoForMania.put(difficoult, difficoultStar);
            }
        }


        beatmapOsuDB.drainTime = readInt();
        beatmapOsuDB.totalTime = readInt();
        beatmapOsuDB.timeWhenAudioPreview = readInt();
        beatmapOsuDB.timePoints = readInt();

        List<Double> timePointsList = new ArrayList<Double>();
        for(int i = 0; i < beatmapOsuDB.timePoints; i++){
            double f = readDouble();
            double g = readDouble();
                    timePointsList.add(g);
            boolean d = readBoolean();
            System.out.print("");
        }
        beatmapOsuDB.beatbapID = readInt();
        beatmapOsuDB.beatmapSetID = readInt();
        beatmapOsuDB.threadID = readInt();
        beatmapOsuDB.gradeAchievedInOsu = readByte();
        beatmapOsuDB.gradeAchievedInTaiko = readByte();
        beatmapOsuDB.gradeAchievedInCTB = readByte();
        beatmapOsuDB.gradeAchievedInMania = readByte();
        beatmapOsuDB.beatmapOfset = readShort();
        beatmapOsuDB.stackLeniency = readSingle();
        beatmapOsuDB.osuMod = GameplayMode.valueOf(readByte(), beatmapOsuDB.beatbapID);
        beatmapOsuDB.songSource = readString();
        beatmapOsuDB.songTags = readString();
        beatmapOsuDB.onlineOffset = readShort();
        beatmapOsuDB.fontUsedTitel = readString();
        beatmapOsuDB.unplayed = readBoolean();
        beatmapOsuDB.lastTimePlayed = readDate();
        beatmapOsuDB.isOsz2 = readBoolean();
        beatmapOsuDB.folderName = readString();
        beatmapOsuDB.lastTimeCheckedAgainsRepository = readDate();
        beatmapOsuDB.ignorBeatmapSound = readBoolean();
        beatmapOsuDB.ignorBeatmapSkin = readBoolean();
        beatmapOsuDB.disableBeatmapStoryboard = readBoolean();
        beatmapOsuDB.disableBeatmapVideo = readBoolean();
        beatmapOsuDB.visualOverwrite = readBoolean();
        if(version < 20140609){
            beatmapOsuDB.unknow = readShort();
        }
        beatmapOsuDB.lastModificationTime = readInt();
        beatmapOsuDB.maniaScrollSpeed = readByte();



        return beatmapOsuDB;
    }




    public enum GameplayMode
    {
        OsuStandard((byte) 0),
        Taiko((byte) 1),
        CTB((byte) 2),
        Mania((byte) 3);

        public final byte byteValue;

        private GameplayMode(byte byteValue)
        {
            this.byteValue = byteValue;
        }

        public static GameplayMode valueOf(byte byteValue, int id)
        {
            for (GameplayMode item : values())
            {
                if (item.byteValue == byteValue) return item;
            }

            throw new IllegalArgumentException("byteValue - Problem ID:" + id);

        }
    }

    public enum RankedStatus
    {
        Ranked((byte) 4),
        Approved((byte) 5),
        Pending((byte) 2),
        Unknow01((byte) 1),
        Unknow03((byte) 3),
        Unknow00((byte) 0),
        Unknow07((byte) 7),
        Unknow08((byte) 8),
        Unknow09((byte) 9),
        Unknow10((byte) 10);

        public final byte byteValue;

        private RankedStatus(byte byteValue)
        {
            this.byteValue = byteValue;
        }

        public static RankedStatus valueOf(byte byteValue)
        {
            for (RankedStatus item : values())
            {
                if (item.byteValue == byteValue) return item;
            }
            throw new IllegalArgumentException("byteValue");
        }
    }



    public enum OsuMod
    {
        NoFail(1),
        Easy(2),
        NoVideo(4),
        Hidden(8),
        HardRock(16),
        SuddenDeath(32),
        DoubleTime(64),
        Relax(128),
        HalfTime(256),
        Nightcore(512),
        Flashlight(1024),
        Autoplay(2048),
        SpunOut(4096),
        Relax2(8192),
        Perfect(16384),
        Key4(32768),
        Key5(65536),
        Key6(131072),
        Key7(262144),
        Key8(524288),
        keyMod(1015808),
        FadeIn(1048576),
        Random(2097152),
        LastMod(4194304);

        public final int bit;

        private OsuMod(int bit)
        {
            this.bit = bit;
        }

        public static EnumSet<OsuMod> valueOf(int bits)
        {
            EnumSet<OsuMod> result = EnumSet.noneOf(OsuMod.class);
            for (OsuMod flag : OsuMod.values())
            {
                if ((bits & flag.bit) == flag.bit)
                {
                    result.add(flag);
                }
            }
            return result;
        }
    }



    public String byteArrayToString(byte[] in) {
        char out[] = new char[in.length * 2];
        for (int i = 0; i < in.length; i++) {
            out[i * 2] = "0123456789ABCDEF".charAt((in[i] >> 4) & 15);
            out[i * 2 + 1] = "0123456789ABCDEF".charAt(in[i] & 15);
        }
        return new String(out);
    }
}